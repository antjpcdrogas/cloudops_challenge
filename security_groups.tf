resource "aws_default_security_group" "awslab-sg-public" {
  vpc_id       = "${aws_vpc.awslab-vpc.id}"
  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port = -1
    to_port = -1
    protocol = "icmp"
}
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
tags = {
    Name = "awslab-SG-Default"
  }
}



###DB SG
resource "aws_security_group" "awslab-sg-db" {
  vpc_id       = "${aws_vpc.awslab-vpc.id}"
  name = "SG database"
  description = "SG database"
  ingress {
    cidr_blocks = ["172.16.1.0/24"]
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
  }
  ingress {
    cidr_blocks = ["172.16.1.0/24"]
    from_port   = 3110
    to_port     = 3110
    protocol    = "tcp"
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "awslab-SG-Database"
  }
}


###WS SG
resource "aws_security_group" "awslab-sg-ws" {
  vpc_id       = "${aws_vpc.awslab-vpc.id}"
  name = "SG WebServer"
  description = "SG WebServer"
  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
  }
  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
tags = {
    Name = "awslab-SG-Webserver"
  }
}