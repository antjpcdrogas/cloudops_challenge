variable "challenge_ami" {
 type = "string"
 description = "AMI used to create webserver and db instance"
 default = "ami-023c8dbf8268fb3ca"
 }


variable "challenge_pubkey" {
 type = "string"
 description = "Public key to access AMIs in this challenge"
 default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCsCall/f9Zr44YBOGKDoROGkdswfUCOZwqJx7zNr3lqWEXP0tsT0t6sRnDw649XJNvBNPD8t/BWnLqJX85v82nMbcGT7rRdvRPIBeUZ+GsG7WqPq7FeuawEcJwc7fySWKUF7pu6dr2xuUf04GDyx9LTZduX5binqzflVDacoe+/OTHpiWARWWxWRQfajxt8QSLhZZMLj9uHfmT5qwm5N3oz189FIKKdIQwZqkRhrO4F/ZahoZsXcASqAiGeCZ7cnHY1P+loGLzNMSyX88kfHxjCHmQchmUyDamXtDZyuHDboLig22pOfmzx1hztjE09Gainm5l5ARmk/TTtZNjlKpH awslab_cocus"
 }
