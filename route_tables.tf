resource "aws_default_route_table" "awslab-rt-igw" {
  default_route_table_id = "${aws_vpc.awslab-vpc.default_route_table_id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.awslab-vpc-gtw.id}"
  }
  tags = {
    Name = "awslab-rt-internet"
  }
}
