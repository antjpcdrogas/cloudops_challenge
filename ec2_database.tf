
resource "aws_network_interface" "awslab-netint-database" {
  subnet_id   = "${aws_subnet.awslab-subnet-private.id}"
  private_ips = ["172.16.2.100"] 
  security_groups = ["${aws_security_group.awslab-sg-db.id}","${aws_default_security_group.awslab-sg-public.id}"]
  tags = {
    Name = "net_int_db"
  }
}




resource "aws_instance" "awslab-ec2-database" {
  ami           = "${var.challenge_ami}" 
  instance_type = "t2.micro"
  key_name = "aws_lab"
  root_block_device {
    volume_type = "gp2"
    volume_size = 8
    delete_on_termination = true ## for databases this value should be false in order to be able to restore the database
  }
  network_interface {
    network_interface_id = "${aws_network_interface.awslab-netint-database.id}"
    device_index         = 0
  }
  tags = {
    Name = "awslab-ec2-Database"
  }
}

