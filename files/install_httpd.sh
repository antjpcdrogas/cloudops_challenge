#! /bin/bash
sudo yum install -y httpd
sudo service httpd start
PRIV_IP=$(/sbin/ifconfig eth0 | grep 'inet addr' | cut -d: -f2 | awk '{print $1}')
echo "<h1>CloudOps COCUS CHALLENGE</h1><tr>Node being used: $PRIV_IP" > /var/www/html/index.html
