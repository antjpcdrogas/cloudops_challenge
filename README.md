1º Create Infra with all required components.

2º Add auto scaling group.

3º Add Elastic LoadBalancer.

4º Clean code and add variables

Improvements out of the challenge:
  - Add RDS instance for Database
  - Create target groups on ASG on multiple AZ
  - Deploy immutable AMIs ready to go to add on ELB
  - PubKey should be read directly from the .pem file using a data resource
  - Improve tag blocks
  - Version should be restricted (= instead of >)


