terraform {
  required_version = "= 0.12"##Version used during the challenge
}

provider "aws" {
  version = "= 2.28" ##Version used during the challenge
  region  = "us-east-2"
}


#VPC
resource "aws_vpc" "awslab-vpc" {
  cidr_block = "172.16.0.0/16"
  tags = {
    Name = "awslab-vpc"
  }
}



#IGW
resource "aws_internet_gateway" "awslab-vpc-gtw" {
  vpc_id = "${aws_vpc.awslab-vpc.id}"
  tags = {
    Name = "Internet"
  }
}



##Public Subnet
resource "aws_subnet" "awslab-subnet-public" {
  vpc_id            = "${aws_vpc.awslab-vpc.id}"
  cidr_block        = "172.16.1.0/24"
  availability_zone = "us-east-2a"
  map_public_ip_on_launch = true
  tags = {
    Name = "pub_subnet"
  }
}



##Private Subnet
resource "aws_subnet" "awslab-subnet-private" {
  vpc_id            = "${aws_vpc.awslab-vpc.id}"
  cidr_block        = "172.16.2.0/24"
  availability_zone = "us-east-2a"
  tags = {
    Name = "priv_subnet"
  }
}



#Key-Pair
resource "aws_key_pair" "awslab-pubkey" {
  key_name   = "aws_lab"
  public_key = "${var.challenge_pubkey}"
}
