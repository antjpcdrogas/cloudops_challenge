resource "aws_autoscaling_group" "awslab-asg-ws" {
  launch_configuration = "${aws_launch_configuration.awslab-launchconf-ws.id}"
  vpc_zone_identifier  = ["${aws_subnet.awslab-subnet-public.id}"]
  min_size = 2
  max_size = 5
  load_balancers = ["${aws_elb.awslab-lb-asg-ws.name}"]
  health_check_type = "ELB"
  tag {
    key = "Name"
    value = "awslab-ASG-ws"
    propagate_at_launch = true
  }
}


  resource "aws_elb" "awslab-lb-asg-ws" {
  name = "awslab-ELB-ws"
  security_groups = ["${aws_default_security_group.awslab-sg-public.id}","${aws_security_group.awslab-sg-ws.id}"]
  subnets = ["${aws_subnet.awslab-subnet-public.id}"]
  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 3
    interval = 30
    target = "HTTP:80/"
  }
  listener {
    lb_port = 80
    lb_protocol = "http"
    instance_port = "80"
    instance_protocol = "http"
  }
}