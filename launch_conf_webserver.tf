resource "aws_launch_configuration" "awslab-launchconf-ws" {
  image_id           = "${var.challenge_ami}"
  security_groups = ["${aws_security_group.awslab-sg-ws.id}","${aws_default_security_group.awslab-sg-public.id}"]
  instance_type = "t2.micro"
  key_name = "aws_lab"
user_data = "${file("files/install_httpd.sh")}"
  root_block_device {
    volume_type = "gp2"
    volume_size = 8
    delete_on_termination = true
  }
  
}